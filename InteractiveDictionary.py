r""" 
Title: Interactive Dictionary

Author: Bruno Rodriguez
Date: 2017-12-30

Purpose: To design a dictionary that searches up the definition of a 
word in a accordance with the input.

Features:
- Takes slightly off words into account
- Will display a message if the word that was input is not a word or 
not in the dictionary
- Takes letters that are uppercase into account so that theres no 
ambiguity

Have Fun!

"""
# importing all libraries that are needed for the program
import json
import datetime
import glob
import difflib
from difflib import SequenceMatcher
from difflib import get_close_matches


data = json.load(open("data.json", "r")) #This line of code puts the content of the json dictionary file into a dictionary variable 'data'
again = ""
print("Welcome to the Interactive Dictionary! \nFollow the instructions and you'll find any definition in no time!\n")
print("\n")

# this function will return the definition from the entered word
def definition(interface, word2):

	if interface in data: # if the first word is typed correctly then it will return a list of definitions from the dictionary
		return data[interface]

	elif len(word2) > 0: # this block of statements checks to see if you mistyped a word and would like to search for a similar word that is in the dictionary
		for i in word2:
			print("No word found, did you mean:", i+"? Enter Y for Yes and N for No and to keep searching matches and NN for No and close a continuous search:")
			true = input() # prompts for user input
			if true == "Y":
				return data[i] #returns the first match's definition
			elif true == "N":
				break # this will evaluate the second loop in which it evaluates the second ,third, fourth, etc closest word to the mistyped word
			elif true == "NN":
				return "Search ended. Sorry :S" # unfort, returns so that it stops the search
		for j in word2: # second loop that evaluates the other potential matches
			print("Did you mean", j+"?")
			true2 = input()
			if true2 == "Y":
				return data[j]
			elif true2 == "N": # if not the second match, try the third match, etc as it just continues the loop
				continue
			elif true2 == "NN":
				return "Search ended or no words that matched were found. Sorry :S" # unfort
		return "Word is not in the dictionary! Sorry!" #default statement if the word ratio is not high enough or the word not in the dictionary

 # this function definition takes all capital letters and converts them to lowercase to account for ambiguity
def uppercase_acc(word_a):
	return word_a.lower() 

# this function retrieves a set of close matching words in the case that a word may have been mistyped
def similarWords(some_word):
	return get_close_matches(some_word, data.keys())


# the 'main' function

while again != "N":
	userInterface = input("Enter a word to search a definition for: ") # this will retrieve the definition of the word in accordance to the user input
	userInterface = uppercase_acc(userInterface) # calls the function that takes uppercase letters into consideration 
	simWord = similarWords(userInterface) #gets a list of possible words upon a potential typo or a word not in the dictionary
	output = definition(userInterface, simWord) #this output is used 
	
	for item in output:

		print("Definition: ",item) # prints the definition of the word
	
	print("\n")
		
	again = input("Did you want to search up another word? Enter Y for Yes and N for No: ") # prompts the user if they need to search up another word
	while again != "Y" and again != "N":
		again = input("Not a valid input. Enter Y or N only: ") #in case the input was not Y or N

	print("\n")
	print("End of program") 

# End of Program
	


