r"""
Title: Webcam Motion Detector

DISCLAIMER: I had help from Udemy.com for help
with the cv2 library and its use, aside from that,
the solution is my own

Author: Bruno Rodriguez

Purpose: Build a Webcam Motion Detector that will
detect one's face during a video capture session
using the cv2 and time libraries


"""
#importing all the libraries needed for the webcam
import cv2
import time



# creating a video variable that will be used to capture video
video = cv2.VideoCapture(0)
#the need for a first frame is necessary due to initial position of the camera recording
first = None

#initializing the infinite loop that will capture the frames of the video
while True:
    check, frame = video.read() # this reads the video frame by frame
    gray = cv2.GaussianBlur(cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY),(21,21),0)
#will need a threshold frame for actual motion detection with Contours
    if first is None:
        first = gray
        continue

    delta = cv2.absdiff(first,gray)
    threshold = cv2.dilate(cv2.threshold(delta, 30, 255, cv2.THRESH_BINARY)[1], None, iterations = 2)

    (_,list_of_contours,_) = cv2.findContours(threshold.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)



    for contour in list_of_contours:
        area= cv2.contourArea(contour)
        if area <10000:
            continue

        (a,b,c,d) = cv2.boundingRect(contour)
        cv2.rectangle(frame, (a,b), (a+c,b+d), (0,200,255),3)

    cv2.imshow("Colour", frame) # displays the content of the video


    #determines the parameter for the frequency of the video, in this case, the max frequency
    key = cv2.waitKey(1)


    if key == ord('h'):
        break



video.release()
cv2.destroyAllWindows
